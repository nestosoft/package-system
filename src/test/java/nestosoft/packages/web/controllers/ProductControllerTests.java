package nestosoft.packages.web.controllers;

import nestosoft.packages.web.resources.ProductResource;
import nestosoft.packages.services.ProductManagementService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class ProductControllerTests {
    ProductController controller;

    @Mock
    ProductManagementService productManagementService;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        controller = new ProductController(productManagementService);
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
    }

    @Test
    public void itShouldReturnProductWhenExists(){
        String productId = "productId";
        ProductResource expected = ProductResource.builder().productId(productId).build();
        when(productManagementService.getProductById(productId)).thenReturn(expected);

        ResponseEntity res = controller.getProduct(productId);

        assertThat(res.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
        assertThat(res.getBody().getClass()).isEqualTo(ProductResource.class);
        ProductResource responseResource = (ProductResource) res.getBody();
        assertThat(responseResource.getProductId()).isEqualTo(productId);
    }

    @Test
    public void itShouldReturnAllProducts(){
        String id = "p1";
        ProductResource expected = ProductResource.builder().productId(id).build();
        List products = new ArrayList();
        products.add(expected);
        when(productManagementService.getAllProducts()).thenReturn(products);

        ResponseEntity res = controller.getAllProducts();

        assertThat(res.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
        List<ProductResource> response = (List<ProductResource>) res.getBody();
        assertThat(response.size()).isEqualTo(1);
        assertThat(response.get(0).getProductId()).isEqualTo(id);
    }

    @Test
    public void itShouldInsertNewProducts(){
        String id = "p1";
        ProductResource toInsert = ProductResource.builder().build();
        ProductResource afterInserted = ProductResource.builder().productId(id).build();
        when(productManagementService.addProduct(toInsert)).thenReturn(afterInserted);

        ResponseEntity res = controller.insertProduct(toInsert);
        ProductResource responseProduct = (ProductResource) res.getBody();
        assertThat(responseProduct.getProductId()).isEqualTo(id);
    }
}
