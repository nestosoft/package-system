package nestosoft.packages.web.controllers;

import nestosoft.packages.services.ProductManagementService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductControllerIntegrationTests {

    @Autowired
    private TestRestTemplate template;

    @MockBean
    private ProductManagementService productManagementService;


    @LocalServerPort
    int localPort;

    @Test
    public void accessingWithCredentialReturnOk() throws Exception {
        template = new TestRestTemplate("user", "pass");

        ResponseEntity response = template.getForEntity("http://localhost:"+localPort+"/product/", Object[].class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void accessingWithWrongCredentialReturnForbidden() throws Exception {
        template = new TestRestTemplate("user2", "pass2");

        ResponseEntity response = template.getForEntity("http://localhost:"+localPort+"/product/", Object[].class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    public void accessingWithOutCredentialReturnForbidden() throws Exception {
        template = new TestRestTemplate();

        ResponseEntity response = template.getForEntity("http://localhost:"+localPort+"/product/", Object[].class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }
}

