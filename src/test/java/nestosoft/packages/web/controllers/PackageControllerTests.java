package nestosoft.packages.web.controllers;

import nestosoft.packages.exceptions.ResourceCollisionException;
import nestosoft.packages.exceptions.ResourceNotFoundException;
import nestosoft.packages.services.CurrencyExchangeService;
import nestosoft.packages.web.resources.PackageResource;
import nestosoft.packages.services.PackageManagementService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PackageControllerTests {
    PackageController controller;

    @Mock
    PackageManagementService packageManagementService;
    @Mock
    CurrencyExchangeService currencyExchangeService;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        controller = new PackageController(packageManagementService, currencyExchangeService);
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
    }

    @Test
    public void itShouldListAllTheExistingPackages(){
        String id = "pack1";
        PackageResource expected = PackageResource.builder().packageId(id).build();
        List<PackageResource> packages = new ArrayList();
        packages.add(expected);
        when(packageManagementService.getAllPackages()).thenReturn(packages);

        ResponseEntity<List<PackageResource>> res = controller.getPackages();

        assertThat(res.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
        List<PackageResource> response = res.getBody();
        assertThat(response.size()).isEqualTo(1);
        assertThat(response.get(0).getPackageId()).isEqualTo(id);
        verify(packageManagementService, times(1)).getAllPackages();
    }

    @Test
    public void itShouldCreateNewPackage(){
        PackageResource toInsert = PackageResource.builder().packageId("newone").description("new product").price(123).build();
        when(packageManagementService.insertPackage(toInsert)).thenReturn(toInsert);

        ResponseEntity<PackageResource> res = controller.createPackage(toInsert);

        assertThat(res.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
        verify(packageManagementService, times(1)).insertPackage(toInsert);
    }

    @Test
    public void itShouldReturnConflictIfAlreadyExisting(){
        PackageResource toInsert = PackageResource.builder().packageId("newone").description("new product").price(123).build();
        when(packageManagementService.insertPackage(toInsert)).thenThrow(new ResourceCollisionException());

        ResponseEntity res = controller.createPackage(toInsert);

        assertThat(res.getStatusCode()).isEqualByComparingTo(HttpStatus.BAD_REQUEST);
        verify(packageManagementService, times(1)).insertPackage(toInsert);
    }

    @Test
    public void itShouldUpdateAPackage(){
        String packid = "packid";
        PackageResource toUpdate = PackageResource.builder().packageId("newone").description("alter description").build();
        when(packageManagementService.updatePackage(packid, toUpdate)).thenReturn(toUpdate);

        ResponseEntity res = controller.updatePackage(packid, toUpdate);

        assertThat(res.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
        verify(packageManagementService, times(1)).updatePackage(packid,toUpdate);
    }

    @Test
    public void itShouldReturnNotFoundWhenTryingToUpdateAPackageThatDoesNotExist(){
        String packid = "packid";
        PackageResource toUpdate = PackageResource.builder().packageId("newone").description("alter description").build();
        when(packageManagementService.updatePackage(packid, toUpdate)).thenThrow(new ResourceNotFoundException());

        ResponseEntity res = controller.updatePackage(packid, toUpdate);

        assertThat(res.getStatusCode()).isEqualByComparingTo(HttpStatus.NOT_FOUND);
        verify(packageManagementService, times(1)).updatePackage(packid,toUpdate);
    }

    @Test
    public void itShouldDeletePackage(){
        String packid = "packid";

        ResponseEntity res = controller.deletePackageById(packid);

        assertThat(res.getStatusCode()).isEqualByComparingTo(HttpStatus.NO_CONTENT);
        verify(packageManagementService, times(1)).deletePackage(packid);
    }

    @Test
    public void itShouldGetPackageById(){
        String packid = "packid";
        PackageResource pack = PackageResource.builder().packageId("pack1").description("description").build();
        when(packageManagementService.getPackageById(packid)).thenReturn(pack);

        ResponseEntity res = controller.getPackageById(packid, null);

        assertThat(res.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
        verify(packageManagementService, times(1)).getPackageById(packid);
    }

    @Test
    public void itShouldReturnNotFoundeWhenPackageDoesNotExists(){
        String packid = "packid";
        PackageResource pack = PackageResource.builder().packageId("pack1").description("description").build();
        when(packageManagementService.getPackageById(packid)).thenThrow(new ResourceNotFoundException());

        ResponseEntity res = controller.getPackageById(packid, null);

        assertThat(res.getStatusCode()).isEqualByComparingTo(HttpStatus.NOT_FOUND);
        verify(packageManagementService, times(1)).getPackageById(packid);
    }

    @Test
    public void itShouldReturnPackageInEuros(){
        String packid = "packid";
        PackageResource pack = PackageResource.builder().packageId("pack1").description("description").build();
        when(packageManagementService.getPackageById(packid)).thenReturn(pack);
        when(currencyExchangeService.calculateCurrencyExchange(any(PackageResource.class), eq("EUR"))).thenReturn(pack);

        ResponseEntity res = controller.getPackageById(packid, "EUR");

        assertThat(res.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
        verify(currencyExchangeService, times(1)).calculateCurrencyExchange(pack, "EUR");
        verify(packageManagementService, times(1)).getPackageById(packid);
    }
}
