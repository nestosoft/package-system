package nestosoft.packages.services;

import nestosoft.packages.domain.Product;
import nestosoft.packages.web.resources.PackageResource;
import nestosoft.packages.web.resources.ProductResource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.TestPropertySource;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class CurrencyExchangeServiceTests {
    CurrencyExchangeService currencyExchangeService;

    @Before
    public void setup(){
        currencyExchangeService = new CurrencyExchangeService();
    }

    @Test
    public void itShouldRecalculatePackageResourceToEuros(){
        List<ProductResource> products = new ArrayList<>();
        products.add(ProductResource.builder().productId("1").price(8).build());
        products.add(ProductResource.builder().productId("2").price(4).build());
        PackageResource pack = PackageResource.builder().price(12).currency("USD").products(products).build();

        PackageResource result = currencyExchangeService.calculateCurrencyExchange(pack,"EUR");

        assertThat(result.getPrice()).isBetween(7, 10);
    }
}
