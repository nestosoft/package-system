package nestosoft.packages.services;


import nestosoft.packages.domain.Product;
import nestosoft.packages.domain.ProductRepository;
import nestosoft.packages.web.controllers.PackageController;
import nestosoft.packages.web.resources.ProductResource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import sun.swing.BakedArrayList;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProductManagementServiceTests {

    ProductManagementService service;
    @Mock
    ProductRepository repository;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        service = new ProductManagementService(repository, new ProductMapper());
    }

    @Test
    public void itShouldListAllTheProducts(){
        List<Product> productEntities = new ArrayList();
        Product p1 = new Product("prod-1", "desc1", 112);
        Product p2 = new Product("prod-2", "desc2", 12);
        productEntities.add(p1);
        productEntities.add(p2);
        when(repository.findAll()).thenReturn(productEntities);

        List<ProductResource> products = service.getAllProducts();

        assertThat(products.size()).isEqualTo(productEntities.size());
        verify(repository, times(1)).findAll();
    }

    @Test
    public void itShouldGetProductById(){
        String productId = "prod-1";
        Product p1 = new Product(productId, "desc1", 112);
        when(repository.findOne(productId)).thenReturn(p1);

        ProductResource product = service.getProductById(productId);

        assertThat(product.getProductId()).isEqualTo(productId);
        verify(repository, times(1)).findOne(productId);
    }

    @Test
    public void itShouldAddProduct(){
        String productId = "prod-23";
        String description = "desct";
        int price = 123;
        ProductResource resource = ProductResource.builder()
                .productId(productId)
                .description(description)
                .price(price)
                .build();
        Product product = new Product(productId, description, price);
        when(repository.save(any(Product.class))).thenReturn(product);

        ProductResource productResource = service.addProduct(resource);

        assertThat(productResource.getProductId()).isEqualTo(productId);
        verify(repository, times(1)).save(any(Product.class));
    }
}
