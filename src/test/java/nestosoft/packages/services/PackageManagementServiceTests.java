package nestosoft.packages.services;

import nestosoft.packages.domain.Package;
import nestosoft.packages.domain.PackageRepository;
import nestosoft.packages.domain.ProductRepository;
import nestosoft.packages.exceptions.ResourceCollisionException;
import nestosoft.packages.exceptions.ResourceNotFoundException;
import nestosoft.packages.web.resources.PackageResource;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PackageManagementServiceTests {
    PackageManagementService service;

    @Mock
    PackageRepository packageRepository;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        service = new PackageManagementService(packageRepository, new PackageMapper(new ProductMapper()));
    }

    @Test
    public void itShouldRetrieveAPackage(){
        String packId = "packId";
        String name = "name";
        String desc = "description";
        Package pack = new Package(packId, name, desc);
        when(packageRepository.findOne(packId)).thenReturn(pack);
        PackageResource expected = PackageResource.builder().packageId(packId).build();

        PackageResource resource = service.getPackageById(packId);

        assertThat(resource.getPackageId()).isEqualTo(expected.getPackageId());
    }

    @Test
    public void itShouldThrowExeptionIfPackageIsNotFound(){
        String packId = "packId";
        when(packageRepository.findOne(packId)).thenReturn(null);

        Throwable thrown = catchThrowable(() -> {
            service.getPackageById(packId);
        });

        Assertions.assertThat(thrown).isInstanceOf(ResourceNotFoundException.class);
    }

    @Test
    public void itShouldGetAllTheAvailablePackages(){
        Package p1 = new Package("pack1", "pack1", "pack1");
        Package p2 = new Package("pack2", "pack2", "pack2");
        List<Package> packages = new ArrayList<>();
        packages.add(p1);
        packages.add(p2);
        when(packageRepository.findAll()).thenReturn(packages);

        List<PackageResource> response = service.getAllPackages();

        assertThat(response.size()).isEqualTo(packages.size());
    }

    @Test
    public void itShouldCreateNewPackage(){
        String packId = "pack1";
        PackageResource resource = PackageResource.builder().packageId(packId).build();
        when(packageRepository.exists(packId)).thenReturn(false);

        PackageResource response = service.insertPackage(resource);

        verify(packageRepository, times(1)).save(any(Package.class));
    }

    @Test
    public void itShouldThrowExceptionIfResourceExistsWhenCreatingNewPackage(){
        String packId = "pack1";
        PackageResource resource = PackageResource.builder().packageId(packId).build();
        when(packageRepository.exists(packId)).thenReturn(true);

        Throwable thrown = catchThrowable(() -> {
            service.insertPackage(resource);
        });

        Assertions.assertThat(thrown).isInstanceOf(ResourceCollisionException.class);
    }

    @Test
    public void itShouldDeletePackage(){
        String packId = "pack1";
        //doNothing().when(packageRepository).delete(packId);

        service.deletePackage(packId);

        verify(packageRepository, times(1)).delete(packId);
    }

    @Test
    public void itShouldUpdatePackage(){
        String packId = "packId";
        PackageResource toUpdate = PackageResource.builder().packageId(packId).description("new desc").build();
        Package pack = new Package(packId, "name", "old desc");
        when(packageRepository.findOne(packId)).thenReturn(pack);

        PackageResource result = service.updatePackage(packId, toUpdate);

        assertThat(result.getDescription()).isEqualTo(toUpdate.getDescription());
        assertThat(result.getName()).isEqualTo(pack.getName());

    }

    @Test
    public void itShouldThrowExeptionIfPackageIsNotFoundWhenUpdating(){
        String packId = "packId";
        PackageResource toUpdate = PackageResource.builder().packageId(packId).description("new desc").build();
        when(packageRepository.findOne(packId)).thenReturn(null);

        Throwable thrown = catchThrowable(() -> {
            service.updatePackage(packId, toUpdate);
        });

        Assertions.assertThat(thrown).isInstanceOf(ResourceNotFoundException.class);
    }
}
