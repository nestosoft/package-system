package nestosoft.packages.config;

import nestosoft.packages.domain.PackageRepository;
import nestosoft.packages.domain.ProductRepository;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackageClasses={PackageRepository.class, ProductRepository.class})
public class RepositoryConfiguration {
}
