package nestosoft.packages.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity(name = "Product")
@Table(name = "product")
@Data
public class Product {
    @Id
    private String productId;
    private String description;
    private Integer price;

    @ManyToMany(mappedBy = "products")
    private List<Package> packages;

    protected Product(){}

    public Product(String productId, String description, Integer price){
        this.productId = productId;
        this.description = description;
        this.price = price;
    }

}
