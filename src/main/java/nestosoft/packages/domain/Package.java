package nestosoft.packages.domain;

import lombok.Data;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "Package")
@Table(name = "package")
@Data
public class Package {
    @Id
    private String packageId;
    private String name;
    private String description;

    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "package_product",
            joinColumns = @JoinColumn(name = "package_id"),
            inverseJoinColumns = @JoinColumn(name = "product_id")
    )
    private List<Product> products;

    protected Package (){}

    public Package(String packageId, String name, String description){
        this.packageId = packageId;
        this.name = name;
        this.description = description;
        this.products = new ArrayList<>();
    }
}
