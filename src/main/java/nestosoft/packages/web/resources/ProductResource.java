package nestosoft.packages.web.resources;

import lombok.Builder;
import lombok.Data;
import org.springframework.hateoas.ResourceSupport;

@Data
@Builder
public class ProductResource extends ResourceSupport {
    String productId;
    String description;
    Integer price;
}
