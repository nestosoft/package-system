package nestosoft.packages.web.resources;

import lombok.Builder;
import lombok.Data;
import org.springframework.hateoas.ResourceSupport;

import java.util.List;

@Data
@Builder
public class PackageResource extends ResourceSupport {
    String packageId;
    String name;
    String description;
    List<ProductResource> products;
    Integer price;
    String currency;
}
