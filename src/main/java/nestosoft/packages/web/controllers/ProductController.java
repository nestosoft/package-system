package nestosoft.packages.web.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nestosoft.packages.web.resources.ProductResource;
import nestosoft.packages.services.ProductManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/product")
@RequiredArgsConstructor
@Slf4j
@PreAuthorize("isAuthenticated()")
public class ProductController {

    @Autowired
    private final ProductManagementService productManagementService;

    @RequestMapping(value="/{productId}", method= RequestMethod.GET)

    ResponseEntity<ProductResource> getProduct(@PathVariable String productId) {
        ProductResource product = productManagementService.getProductById(productId);
        return ResponseEntity.ok(product);
    }

    @RequestMapping(value="/", method= RequestMethod.GET)
    ResponseEntity<List<ProductResource>> getAllProducts() {
        List<ProductResource> products = productManagementService.getAllProducts();
        return ResponseEntity.ok(products);
    }

    @RequestMapping(value="/", method= RequestMethod.POST)
    public ResponseEntity<ProductResource> insertProduct(@Valid @RequestBody ProductResource productResource) {
        ProductResource product = productManagementService.addProduct(productResource);
        return ResponseEntity.ok(product);
    }


}
