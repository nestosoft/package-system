package nestosoft.packages.web.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nestosoft.packages.exceptions.ResourceCollisionException;
import nestosoft.packages.exceptions.ResourceNotFoundException;
import nestosoft.packages.services.CurrencyExchangeService;
import nestosoft.packages.web.resources.PackageResource;
import nestosoft.packages.services.PackageManagementService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/package")
@RequiredArgsConstructor
@Slf4j
public class PackageController {
    private final PackageManagementService packageManagementService;
    private final CurrencyExchangeService currencyExchangeService;

    @RequestMapping(value="/", method= RequestMethod.GET)
    public ResponseEntity<List<PackageResource>> getPackages() {
        List<PackageResource> packages = packageManagementService.getAllPackages();
        return ResponseEntity.ok(packages);
    }

    @RequestMapping(value="/", method= RequestMethod.POST)
    public ResponseEntity<PackageResource> createPackage(@RequestBody PackageResource packageResource){
        try {
            PackageResource res = packageManagementService.insertPackage(packageResource);
            return ResponseEntity.ok(packageResource);
        }catch (ResourceCollisionException e){
            return ResponseEntity.badRequest().build();
        }
    }

    @RequestMapping(value="/{packageId}", method= RequestMethod.GET)
    public ResponseEntity<PackageResource> getPackageById(@PathVariable String packageId, @RequestParam(value = "currency", required = false) String currency){
        try {
            PackageResource pack = packageManagementService.getPackageById(packageId);
            if (currency != null) currencyExchangeService.calculateCurrencyExchange(pack, currency);
            return ResponseEntity.ok(pack);
        } catch (ResourceNotFoundException e){
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value="/{packageId}", method= RequestMethod.DELETE)
    public ResponseEntity<Void> deletePackageById(@PathVariable String packageId){
        packageManagementService.deletePackage(packageId);
        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value="/{packageId}", method= RequestMethod.PUT)
    public ResponseEntity<PackageResource> updatePackage(@PathVariable String packageId, @RequestBody PackageResource packageResource){
        try {
            PackageResource updated = packageManagementService.updatePackage(packageId, packageResource);
            return ResponseEntity.ok(updated);
        } catch (ResourceNotFoundException e){
            return ResponseEntity.notFound().build();
        }
    }
}
