package nestosoft.packages.services;

import lombok.AllArgsConstructor;
import nestosoft.packages.domain.Product;
import nestosoft.packages.domain.ProductRepository;
import nestosoft.packages.web.resources.ProductResource;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@AllArgsConstructor
public class ProductManagementService {
    private final ProductRepository productRepository;
    private final ProductMapper productMapper;

    public ProductResource getProductById(String id) {
        return productMapper.toResource(productRepository.findOne(id));
    }

    public List<ProductResource> getAllProducts() {
        Iterable<Product> iterableProducts = productRepository.findAll();
        return StreamSupport.stream(iterableProducts.spliterator(), false)
                .map(product -> productMapper.toResource(product))
                .collect(Collectors.toList());

    }

    public ProductResource addProduct(ProductResource productResource) {
        Product product = productMapper.toEntity(productResource);
        productRepository.save(product);
        return productResource;
    }
}
