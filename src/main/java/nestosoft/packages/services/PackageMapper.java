package nestosoft.packages.services;

import lombok.AllArgsConstructor;
import nestosoft.packages.domain.Package;
import nestosoft.packages.domain.Product;
import nestosoft.packages.web.resources.PackageResource;
import nestosoft.packages.web.resources.ProductResource;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Component
public class PackageMapper {
    private final ProductMapper productMapper;

    public PackageResource toResoure(Package pack){
        return PackageResource.builder()
                .packageId(pack.getPackageId())
                .description(pack.getDescription())
                .name(pack.getName())
                .products(toResourceList(pack.getProducts()))
                .price(calculateTotalPrice(pack.getProducts()))
                .currency("USD") //by default
                .build();
    }

    public Package toEntity(PackageResource resource){
        Package pack  = new Package(resource.getPackageId(), resource.getName(), resource.getDescription());
        if (resource.getProducts() != null){
            List<Product> products = resource.getProducts().stream().map(productMapper::toEntity).collect(Collectors.toList());
            pack.setProducts(products);
        }
        return pack;
    }

    private List<ProductResource> toResourceList(List<Product> products){
        return products.stream().map(productMapper::toResource).collect(Collectors.toList());
    }

    private Integer calculateTotalPrice(List<Product> products){
        return products.stream().mapToInt(Product::getPrice).sum();
    }

}
