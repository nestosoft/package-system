package nestosoft.packages.services;

import com.sun.xml.internal.bind.v2.runtime.reflect.Lister;
import lombok.AllArgsConstructor;
import nestosoft.packages.domain.Package;
import nestosoft.packages.domain.PackageRepository;
import nestosoft.packages.domain.Product;
import nestosoft.packages.exceptions.ResourceCollisionException;
import nestosoft.packages.exceptions.ResourceNotFoundException;
import nestosoft.packages.web.resources.PackageResource;
import nestosoft.packages.web.resources.ProductResource;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@AllArgsConstructor
public class PackageManagementService {
    private final PackageRepository packageRepository;
    private final PackageMapper packageMapper;

    public List<PackageResource> getAllPackages() {
        Iterable<Package> packages = packageRepository.findAll();
        return StreamSupport.stream(packages.spliterator(), false)
                .map(product -> packageMapper.toResoure(product))
                .collect(Collectors.toList());
    }

    public PackageResource insertPackage(PackageResource toInsert) {
        Package pack = packageMapper.toEntity(toInsert);
        if (packageRepository.exists(pack.getPackageId())) throw new ResourceCollisionException();
        packageRepository.save(pack);
        return packageMapper.toResoure(pack) ;
    }

    public void deletePackage(String packid) {
        packageRepository.delete(packid);
    }

    public PackageResource updatePackage(String packid, PackageResource toUpdate) {
        Package pack = packageRepository.findOne(packid);
        if (pack == null) throw new ResourceNotFoundException();
        if (toUpdate.getDescription() != null) pack.setDescription(toUpdate.getDescription());
        if (toUpdate.getName() != null) pack.setName(toUpdate.getName());
        if (toUpdate.getProducts() != null) updateProducts(pack, toUpdate);
        packageRepository.save(pack);
        return packageMapper.toResoure(pack);
    }

    private void updateProducts(Package pack, PackageResource resource) {
        Package packFromResouce =  packageMapper.toEntity(resource);
        pack.setProducts(packFromResouce.getProducts());
    }

    public PackageResource getPackageById(String packid) {
        Package pack = packageRepository.findOne(packid);
        if (pack != null) return packageMapper.toResoure(pack);
        throw new ResourceNotFoundException();
    }
}
