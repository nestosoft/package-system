package nestosoft.packages.services;

import nestosoft.packages.domain.Product;
import nestosoft.packages.web.resources.ProductResource;
import org.springframework.stereotype.Component;

@Component
public class ProductMapper {
    public ProductResource toResource(Product product){
        return ProductResource.builder()
                .productId(product.getProductId())
                .description(product.getDescription())
                .price(product.getPrice())
                .build();
    }

    public Product toEntity(ProductResource resource){
        return new Product(resource.getProductId(), resource.getDescription(), resource.getPrice());
    }
}
