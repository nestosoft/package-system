package nestosoft.packages.services;

import nestosoft.packages.domain.Package;
import nestosoft.packages.web.resources.PackageResource;
import org.springframework.stereotype.Service;

@Service
public class CurrencyExchangeService {

    public PackageResource calculateCurrencyExchange(PackageResource pack, String currency){
        Double rate = currencyExchange(currency);
        if (rate !=0.0){
            // update prices for products
            pack.getProducts().stream().forEach(p -> {
                Double newPrice = p.getPrice()*rate;
                p.setPrice(newPrice.intValue());
            });
            // sum all the products
            pack.setPrice(pack.getProducts().stream().mapToInt(p -> p.getPrice()).sum());
            // add the currency
            pack.setCurrency(currency);
            return pack;
        }
        pack.setCurrency("USD");
        return pack;
    }

    private Double currencyExchange(String currency){
        // simulation of currency exchange rate service
        // need to refactor and plug a external system that provide this data dynamically
        switch (currency){
            case "GBP": return 0.78;
            case "EUR": return 0.87;
            default: return 0.0;
        }
    }




}
