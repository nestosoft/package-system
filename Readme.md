# Simple system to manage package of products.

## To compile and run the tests
```bash
mvn clean verify
```
## To run the server
```bash 
mvn spring-boot:run
```
# End points
- create products : ```curl -u user:pass http://localhost:8080/product/  --header "Content-Type: application/json" --request POST --data '{"productId":"product-1","description":"description", "price": 12}'```

- list products: ```curl -u user:pass http://localhost:8080/product/```
- list individual products : ```curl -u user:pass http://localhost:8080/product/```

- create package:  ```curl http://localhost:8080/package/ --header "Content-Type: application/json" --request POST --data '{"packageId":"package-1", "name":"my package", "description": "test package"}'```
- update package:   ```curl -X PUT http://localhost:8080/package/package-1 -H 'content-type: application/json' -d '{"products": [{"productId":"product-1","description":"description", "price": 12}]}'``` 
- list all packages: ```curl http://localhost:8080/package/```
- retrieve package:  ```curl http://localhost:8080/package/package-1```
- retrieve package with currency: ```curl http://localhost:8080/package/package-1?currency=EUR```
- delete package ```curl http://localhost:8080/package/package-1 --request DELETE```


# TODO:

- liquibase to manage database migrations
- Test the PackageMapper and currencyExchangeService (a bit of a rush right now, just wanted to finish a working service)
- swagger for documentation so I don't have to do it by hand.
